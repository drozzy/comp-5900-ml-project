# Taken from
# https://github.com/naokishibuya/deep-learning/blob/master/python/gan_mnist.ipynb

import numpy as np
import keras
import os
import neat
from sklearn.metrics import mean_squared_error

def load_data():
    (X_train_raw, y_train), (X_test_raw, y_test) = keras.datasets.mnist.load_data()
    X_train = preprocess(X_train_raw)
    X_test   = preprocess(X_test_raw)
    
    # Select a random subset of 100 images
    idx = np.random.choice(np.arange(len(y_train)), 100, replace=False)
    X_train = X_train[idx]
    y_train = y_train[idx]


    return ((X_train, y_train), (X_test, y_test))

def main(config_path):
    (X_train, y_train), (X_test, y_test) = load_data()

    def eval_genome(genome, config):
        """
        This function will be run in threads by ThreadedEvaluator.  It takes two
        arguments (a single genome and the genome class configuration data) and
        should return one float (that genome's fitness).
        """
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        i = 0
        error = 0.0
        outputs = []
        for x, y in zip(X_train, y_train):
            [output] = net.activate(x)
            outputs.append(output)
            # error += np.sqrt(mean_squared_error([y], output))

        # print("outputs shape: %s" % outputs)
        mse = mean_squared_error(y_train, outputs)
        # print("MSE is: %s" % mse)
        # exit(0)
        rmse = np.sqrt(mse)
        # error = error / (len(y_train))
        # print("Error is: %s" % error)
        # fitness = 1.0 / (1.0 + error)
        fitness = - rmse
        # print("Fitness is: %s" % fitness)
        return fitness

    def eval_genomes(genomes, config):
        print("Inside eval genomes...")
        for genome_id, genome in genomes:
            genome.fitness = 0.0
            net = neat.nn.FeedForwardNetwork.create(genome, config)
            i = 0

            for x, y in zip(X_train, y_train):
                i +=1
                if (i % 100 == 0):
                    print("iteration %s/%s" % (i, num_obs))
                output = net.activate(x)
                genome.fitness += mean_squared_error(output, [y])


    # Extract above ^ into func...
    print("Configing...")
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)

    # Create the population, which is the top-level object for a NEAT run.
    print("Creating population...")
    p = neat.Population(config)
    # Add a stdout reporter to show progress in the terminal.
    print("Adding reporter...")
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    ### START THREAD
    # Run for up to 300 generations.
    pe = neat.ThreadedEvaluator(4, eval_genome)
    winner = p.run(pe.evaluate, 300)
    pe.stop()
    ### END THREAD

    # # p.add_reporter(neat.Checkpointer(5))
    # # print("Reporter added.")
    # # Run for up to 300 generations.
    # winner = p.run(eval_genomes, 10)
    # # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    
def evaluator():
 
    return eval_genomes

    
def preprocess(x):    
    x = x.reshape(-1, 784) # 784=28*28
    x = np.float64(x)
    x = (x / 255 - 0.5) * 2
    x = np.clip(x, -1, 1)
    return x

if __name__ == '__main__':
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'neat-mnist.config')
    main(config_path)
