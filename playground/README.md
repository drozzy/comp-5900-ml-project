Creates an environment with numpy and friends installed.

To start simply:

1. Install docker (from official sources)

2. Run this command, which will copy the `src` dir and place you in it:

    sh run-bash.sh