import pandas

def main():
    data = tweets_and_annotators()
    print(data.shape)
    print(data.columns)


def tweets_and_annotators():
    """
    Uses pandas to join the tweets with the annotations, returning data frame
    with columns ['id', 'tweet', 'annotator1', 'annotator2']
    """
    df = pandas.read_json("dataset/tweets.json", lines=True)
    df2 = pandas.read_csv("dataset/60Users_annotations.csv")
    m = df[['id', 'text']].merge(df2[['id', 'annotator1', 'annotator2']], on='id')
    return m.rename({'text': 'tweet'}, axis='columns')


if __name__ == '__main__':
    main()