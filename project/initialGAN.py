# Taken from
# https://github.com/naokishibuya/deep-learning/blob/master/python/gan_mnist.ipynb
import sys
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import text_cleaning
import numpy as np
import keras.backend as K
from keras.layers import Input, Dense, Activation, LeakyReLU, SimpleRNN, LSTM, Embedding, TimeDistributed
from keras.models import Sequential
from keras.optimizers import Adam
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import pandas
import os
import random

window_length = 200             # experimental: max number of words in preprocessed tweets in this data set
n_features = 300                # 300 dimensional FastText word Vectors
classes = ['normal','anomalous']            # types of classes that we have in the data
model_directory = '/home/norberteke/'       # pre-trained word embedding model directory
NUMPY_LOAD_VECS = 'models/cc.en.300.npy'


def write_embeddings_to_file(data, filename):
    # Write the array to disk
    with open('embeddings/' + filename + ".txt", 'w') as outfile:
        # I'm writing a header here just for the sake of readability
        # Any line starting with "#" will be ignored by numpy.loadtxt
        outfile.write('# Array shape: {0}\n'.format(data.shape))

        # Iterating through a ndimensional array produces slices along
        # the last axis. This is equivalent to data[i,:,:] in this case
        for data_slice in data:
            # The formatting string indicates that I'm writing out
            # the values in left-justified columns 7 characters in width
            # with 2 decimal places.
            np.savetxt(outfile, data_slice, fmt='%-7.4f')

            # Writing out a break to indicate different slices...
            outfile.write('# New slice\n')


def read_embeddings_from_file(filename, original_shape_1stDim):
    # Read the array from disk
    new_data = np.loadtxt(filename)

    # However, going back to 3D is easy if we know the
    # original shape of the array
    new_data = new_data.reshape((original_shape_1stDim, 32, 300))
    return new_data


def train_test_splits(texts, labels, VALIDATION_SPLIT):
    """
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(texts)
    sequences = tokenizer.texts_to_sequences(texts)

    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH, padding='post')
    """

    # we can shuffle the texts, since they all have the same label, so no need to keep indices
    random.shuffle(texts)  # randomly shuffle data
    nb_validation_samples = int(VALIDATION_SPLIT * len(texts))

    # split the data into a training set and a validation set
    x_train = texts[:-nb_validation_samples]
    y_train = labels[:-nb_validation_samples]
    x_val = texts[-nb_validation_samples:]
    y_val = labels[-nb_validation_samples:]

    return x_train, y_train, x_val, y_val


def make_embedding_layer(texts, read_emb_weights = False):
    MAX_SEQUENCE_LENGTH = 200
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(texts)
    sequences = tokenizer.texts_to_sequences(texts)

    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH, padding='post')

    # assemble the embedding_weights in one numpy array
    vocab_dim = 300  # dimensionality of your word vectors
    n_symbols = len(word_index) + 1  # adding 1 to account for 0th index (for masking)

    if read_emb_weights == False:
        print("started loading in FastText")
        embeddings_index = {}
        f = open(os.path.join(model_directory, 'crawl-300d-2M.vec'))
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        f.close()
        print("finished loading in FastText")

        embedding_weights = np.zeros((n_symbols, vocab_dim))
        for word, index in word_index.items():
            try:
                embedding = embeddings_index[word]
            except:
                continue
            if embedding is not None:
                embedding_weights[index, :] = embedding
        np.save(NUMPY_LOAD_VECS, embedding_weights)
    else:
        embedding_weights = np.load(NUMPY_LOAD_VECS)

    # define inputs here
    embedding_layer = Embedding(len(word_index) + 1, vocab_dim,
                            weights=[embedding_weights],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=True)

    return embedding_layer, data, word_index, n_symbols


def main():
    # pre-process the text
    reviews = pd.read_csv("dataset/depression.csv", encoding="ISO-8859-1", header=None,
                                  names=["id", "text", "label"])

    # pre-process the text
    print("started pre-processing")
    reviews['preprocessed'] = reviews['text'].apply(text_cleaning.preproc)
    print("ended pre-processing")

    anomalousData = list(reviews['preprocessed'])
    anomalous_labels = [1] * len(anomalousData)

    embedding_layer, padded_data, word_index, vocab_size = make_embedding_layer(anomalousData, read_emb_weights=True)

    # create train/test splits
    X_train, y_train, X_test, y_test = train_test_splits(texts=padded_data, labels=anomalous_labels,
                                                         VALIDATION_SPLIT=0.05)
    X_train_real = X_train
    X_test_real = X_test

    # define hyper-parameters
    sample_size = 10  # latent sample size (i.e., 100 random numbers)
    g_hidden_size = n_features
    d_hidden_size = n_features
    leaky_alpha = 0.01
    g_learning_rate = 0.0001  # learning rate for the generator
    d_learning_rate = 0.0001  # learning rate for the discriminator
    epochs = 100
    batch_size = 64  # train batch size
    eval_size = 16  # evaluate size
    smooth = 0.1

    # labels for the batch size and the test size
    y_train_real, y_train_fake = make_labels(batch_size)
    y_eval_real, y_eval_fake = make_labels(eval_size)

    # create a GAN, a generator and a discriminator
    gan, generator, discriminator = make_simple_GAN(
        sample_size,
        g_hidden_size,
        d_hidden_size,
        leaky_alpha,
        g_learning_rate,
        d_learning_rate,
        embedding_layer,
        vocab_size)

    losses = []
    for e in range(epochs):
        for i in range(len(X_train_real) // batch_size):
            # real MNIST digit images
            X_batch_real = X_train_real[i * batch_size:(i + 1) * batch_size]

            # latent samples and the generated digit images
            latent_samples = make_latent_samples(batch_size, sample_size, window_length)
            X_batch_fake = generator.predict_on_batch(latent_samples)

            # train the discriminator to detect real and fake images
            make_trainable(discriminator, True)
            discriminator.train_on_batch(X_batch_real, y_train_real * (1 - smooth))
            discriminator.train_on_batch(X_batch_fake, y_train_fake)

            # train the generator via GAN
            make_trainable(discriminator, False)
            gan.train_on_batch(latent_samples, y_train_real)

        # evaluate
        X_eval_real = X_test_real[np.random.choice(len(X_test_real), eval_size, replace=False)]

        latent_samples = make_latent_samples(eval_size, sample_size, window_length)
        X_eval_fake = generator.predict_on_batch(latent_samples)

        d_loss = discriminator.test_on_batch(X_eval_real, y_eval_real)
        d_loss += discriminator.test_on_batch(X_eval_fake, y_eval_fake)
        g_loss = gan.test_on_batch(latent_samples, y_eval_real)  # we want the fake to be realistic!

        losses.append((d_loss, g_loss))

        print("Epoch: {:>3}/{} Discriminator Loss: {:>6.4f} Generator Loss: {:>6.4f}".format(
            e + 1, epochs, d_loss, g_loss))

        if e % 10 == 0:
            print()
            print("Epoch: ", e)
            examples = 5
            latent_samples = make_latent_samples(examples, sample_size, window_length)
            generator_output = generator.predict_on_batch(latent_samples)

            for output in generator_output:
                text = []
                for vecs in output:
                    text.append(word_index[np.argmax(vecs) + 1])
                print(" ".join(text))



def make_simple_GAN(sample_size,
                    g_hidden_size,
                    d_hidden_size,
                    leaky_alpha,
                    g_learning_rate,
                    d_learning_rate,
                    embedding_layer,
                    vocab_size):
    K.clear_session()

    '''
    generator = Sequential([
        Dense(g_hidden_size, input_shape=(sample_size,)),
        LeakyReLU(alpha=leaky_alpha),
        Dense(784),
        Activation('tanh')
    ], name='generator')
    
    discriminator = Sequential([
        Dense(d_hidden_size, input_shape=(784,)),
        LeakyReLU(alpha=leaky_alpha),
        Dense(1),
        Activation('sigmoid')
    ], name='discriminator')
    '''

    # create and fit
    generator = Sequential([
        Dense(units=sample_size, input_shape=(window_length, sample_size)),
        LeakyReLU(alpha=leaky_alpha),
        SimpleRNN(units=g_hidden_size, return_sequences=True),
        TimeDistributed(Dense(vocab_size)),
        Activation('softmax')
    ], name='generator')

    # create and fit the SimpleRNN model
    discriminator = Sequential([
        embedding_layer, # input: batch_size  * max_seq  .... output: batch_size  * max_seq * dim,
        SimpleRNN(units=d_hidden_size),  # output: batch_size * (1) * hidden_size
        #Dense(d_hidden_size),
        #LeakyReLU(alpha=leaky_alpha),
        Dense(1),
        Activation('sigmoid')
    ], name='discriminator')

    print("Generator Summary:")
    print(generator.summary())
    print()
    print("Discriminator Summary:")
    print(discriminator.summary())


    gan = Sequential([
        generator,
        discriminator
    ])

    discriminator.compile(optimizer=Adam(lr=d_learning_rate), loss='binary_crossentropy')
    gan.compile(optimizer=Adam(lr=g_learning_rate), loss='binary_crossentropy')

    return gan, generator, discriminator


def make_labels(size):
    return np.ones([size, 1]), np.zeros([size, 1])


def make_trainable(model, trainable):
    for layer in model.layers:
        layer.trainable = trainable


def make_latent_samples(n_samples, sample_size, seq_size):
    # return np.random.uniform(-1, 1, size=(n_samples, sample_size))
    return np.random.normal(loc=0, scale=1, size=(n_samples, seq_size, sample_size))

def get_index(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


if __name__ == '__main__':
    main()