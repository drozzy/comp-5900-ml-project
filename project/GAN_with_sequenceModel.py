# Taken from
# https://github.com/naokishibuya/deep-learning/blob/master/python/gan_mnist.ipynb
import sys
import seq2seq
from seq2seq.models import SimpleSeq2Seq
from text_cleaning import preproc as my_preprocessing
from text_cleaning import __autoCorrect as correct_spelling
import preprocessor as prep  # for usage check https://pypi.org/project/tweet-preprocessor/
import numpy as np
import keras
import fasttext
import keras.backend as K
from keras.layers import Input, Dense, Activation, LeakyReLU, BatchNormalization, Dropout, SimpleRNN, LSTM, \
    Bidirectional
from keras.models import Sequential
from keras.optimizers import Adam
import pandas
import os
import random

window_length = 32  # experimental: max number of words in preprocessed tweets in this data set
n_features = 300  # 300 dimensional FastText word Vectors
classes = ['normal', 'anomalous']  # types of classes that we have in the data
model_directory = '/home/norberteke/'  # pre-trained word embedding model directory


def tweets_and_annotators():
    """
    Uses pandas to join the tweets with the annotations, returning data frame
    with columns ['id', 'tweet', 'annotator1', 'annotator2']

    TO DO:
    - join 'annotator1', 'annotator2' values into 1 overall label, called 'label'
    - make the function return two data frames, training and test set, each containing ['id', 'tweet', 'label']
    """

    df = pandas.read_json("dataset/tweets.json", lines=True)
    df2 = pandas.read_csv("dataset/60Users_annotations.csv")
    m = df[['id', 'text']].merge(df2[['id', 'annotator1', 'annotator2']], on='id')
    m = m.rename({'text': 'tweet'}, axis='columns')

    isAnomaly1 = m['annotator1'] > 0
    isAnomaly2 = m['annotator2'] > 0

    isAnomaly = isAnomaly1 | isAnomaly2

    anomalousData = m[isAnomaly]
    normalData = m[~isAnomaly]

    '''
    Test and train splitting
    #m['id'].apply(str)

    #testingSplits_df = pandas.read_csv("dataset/tweetLevel_testingSplit.csv")
    #testing = testingSplits_df[['id']].merge(m[['id', 'tweet', 'annotator1', 'annotator2']], on='id')

    #trainingSplits_df = pandas.read_csv("dataset/tweetLevel_trainingSplit.csv")
    #training = trainingSplits_df[['id']].merge(m[['id', 'tweet', 'annotator1', 'annotator2']], on='id')
    '''

    return normalData, anomalousData


def train_test_splits(texts, labels, VALIDATION_SPLIT):
    """
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(texts)
    sequences = tokenizer.texts_to_sequences(texts)

    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH, padding='post')
    """

    # we can shuffle the texts, since they all have the same label, so no need to keep indices
    random.shuffle(texts)  # randomly shuffle data
    nb_validation_samples = int(VALIDATION_SPLIT * len(texts))

    # split the data into a training set and a validation set
    x_train = texts[:-nb_validation_samples]
    y_train = labels[:-nb_validation_samples]
    x_val = texts[-nb_validation_samples:]
    y_val = labels[-nb_validation_samples:]

    return x_train, y_train, x_val, y_val


def text_to_vector(texts, ft_model):
    """
    Given a list of strings, normalize them, then splits it into words and finally converts
    it to a sequence of word vectors.

    Input: list of strings containing each pre-processed tweet
    Output: numpy array, shape (number_of_tweets, window_length, n_features)
            now that's [x,32,300] ... x = however many tweets in training/test set
    """

    number_of_tweets = len(texts)
    x = np.zeros((number_of_tweets, window_length, n_features))

    for tweet_i, tweet in enumerate(texts):
        words = tweet.split(" ")
        exiting_words = []

        # only use words that are present in the FastText vocabulary. For now remove unknown words
        for word in words:
            try:
                ft_model[word].astype('float32')
                exiting_words.append(word)
            except KeyError:
                try:
                    ft_model[correct_spelling(word)].astype('float32')
                    exiting_words.append(correct_spelling(word))
                except KeyError:
                    continue

        window = exiting_words[-window_length:]

        for word_i, word in enumerate(window):
            x[tweet_i, word_i, :] = ft_model[word].astype('float32')  # word padding is happening here
        return x


def write_embeddings_to_file(data, filename):
    # Write the array to disk
    with open('embeddings/' + filename + ".txt", 'w') as outfile:
        # I'm writing a header here just for the sake of readability
        # Any line starting with "#" will be ignored by numpy.loadtxt
        outfile.write('# Array shape: {0}\n'.format(data.shape))

        # Iterating through a ndimensional array produces slices along
        # the last axis. This is equivalent to data[i,:,:] in this case
        for data_slice in data:
            # The formatting string indicates that I'm writing out
            # the values in left-justified columns 7 characters in width
            # with 2 decimal places.
            np.savetxt(outfile, data_slice, fmt='%-7.4f')

            # Writing out a break to indicate different slices...
            outfile.write('# New slice\n')


def read_embeddings_from_file(filename, original_shape_1stDim):
    # Read the array from disk
    new_data = np.loadtxt(filename)

    # However, going back to 3D is easy if we know the
    # original shape of the array
    new_data = new_data.reshape((original_shape_1stDim, 32, 300))
    return new_data


def find_nearest_neighbor(query, vectors, cossims=None):
    """
    query:  1d numpy array corresponding to the vector to which you want to find the closest vector
    vectors: 2d numpy array corresponding to the vectors you want to consider
    cossims: 1d numpy array of size len(vectors), which can be passed for efficiency

    returns the index of the closest match to query within vectors
    """
    if cossims is None:
        cossims = np.matmul(vectors, query, out=cossims)
    else:
        np.matmul(vectors, query, out=cossims)
    rank = len(cossims) - 1
    result_i = np.argpartition(cossims, rank)[rank]
    return result_i


def main():
    read_embeddings = True
    if read_embeddings is False:
        # define set-up for pre-processor
        prep.set_options(prep.OPT.URL, prep.OPT.EMOJI, prep.OPT.SMILEY,
                         prep.OPT.RESERVED, prep.OPT.MENTION)  # customize what kind of data pre-processing you want

        # get the data from csv and json files
        normalData, anomalousData = tweets_and_annotators()

        # pre-process the text
        print("started pre-processing")
        normalData['preprocessed_tweet'] = normalData['tweet'].apply(lambda x: my_preprocessing(prep.clean(x)))
        anomalousData['preprocessed_tweet'] = anomalousData['tweet'].apply(lambda x: my_preprocessing(prep.clean(x)))
        print("ended pre-processing")

        # store pre-processed data into a more convenient data structure ... a list of tweets
        anomalous_tweets = list(anomalousData['preprocessed_tweet'])
        normal_tweet = list(normalData['preprocessed_tweet'])
        labels = []
        for i in range(0, len(anomalous_tweets)):
            labels.append(1)

        print("Num of normal: %s, Num of anomalous: %s" % (len(normal_tweet), len(anomalous_tweets)))

        # create train/test splits
        X_train, y_train, X_test, y_test = train_test_splits(texts=anomalous_tweets, labels=labels,
                                                             VALIDATION_SPLIT=0.1)

        # load in FastText model
        print("started loading in FastText")
        embeddings_index = {}
        f = open(os.path.join(model_directory, 'crawl-300d-2M.vec'))
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        f.close()
        print("finished loading in FastText")

        """
            get a 2d numpy matrix containing all the word vectors for the vocabulary of a data set
            and a dictionary of indices, where you can look up what word vector index maps to what word

            use case : look up to what word does the nearest vector of a query vector map to 
            nn_vector_index = find_nearest_neighbor(query_vector, embedding_matrix)
            nn_word = embeddings_index_word_lookup[nn_vector_index]
        """

        # convert text data to sequence of word vectors
        X_train_real = text_to_vector(texts=X_train, ft_model=embeddings_index)
        X_test_real = text_to_vector(texts=X_test, ft_model=embeddings_index)

        write_embeddings_to_file(X_train_real, filename="X_train_real_" + str(X_train_real.shape[0]))
        write_embeddings_to_file(X_test_real, filename="X_test_real_" + str(X_test_real.shape[0]))
    else:
        X_train_real = read_embeddings_from_file("embeddings/X_train_real_493.txt", 493)
        X_test_real = read_embeddings_from_file("embeddings/X_test_real_54.txt", 54)

    print()
    print("Shape of X_train_real:", X_train_real.shape)
    print("Shape of X_test_real: ", X_test_real.shape)
    print()

    # define hyper-parameters
    sample_size = 100  # latent sample size (i.e., 100 random numbers)
    g_hidden_size = n_features
    d_hidden_size = n_features
    leaky_alpha = 0.01
    g_learning_rate = 0.0001  # learning rate for the generator
    d_learning_rate = 0.001  # learning rate for the discriminator
    epochs = 200
    batch_size = 64  # train batch size
    eval_size = 16  # evaluate size
    smooth = 0.1

    # labels for the batch size and the test size
    y_train_real, y_train_fake = make_labels(batch_size)
    y_eval_real, y_eval_fake = make_labels(eval_size)

    # create a GAN, a generator and a discriminator
    gan, generator, discriminator = make_simple_GAN(
        sample_size,
        g_hidden_size,
        d_hidden_size,
        leaky_alpha,
        g_learning_rate,
        d_learning_rate)

    losses = []
    for e in range(epochs):
        for i in range(len(X_train_real) // batch_size):
            # real MNIST digit images
            X_batch_real = X_train_real[i * batch_size:(i + 1) * batch_size]

            # latent samples and the generated digit images
            latent_samples = make_latent_samples(batch_size, sample_size, window_length)
            X_batch_fake = generator.predict_on_batch(latent_samples)

            # train the discriminator to detect real and fake images
            make_trainable(discriminator, True)
            discriminator.train_on_batch(X_batch_real, y_train_real * (1 - smooth))
            discriminator.train_on_batch(X_batch_fake, y_train_fake)

            # train the generator via GAN
            make_trainable(discriminator, False)
            gan.train_on_batch(latent_samples, y_train_real)

        # evaluate
        X_eval_real = X_test_real[np.random.choice(len(X_test_real), eval_size, replace=False)]

        latent_samples = make_latent_samples(eval_size, sample_size, window_length)
        X_eval_fake = generator.predict_on_batch(latent_samples)

        d_loss = discriminator.test_on_batch(X_eval_real, y_eval_real)
        d_loss += discriminator.test_on_batch(X_eval_fake, y_eval_fake)
        g_loss = gan.test_on_batch(latent_samples, y_eval_real)  # we want the fake to be realistic!

        losses.append((d_loss, g_loss))

        print("Epoch: {:>3}/{} Discriminator Loss: {:>6.4f} Generator Loss: {:>6.4f}".format(
            e + 1, epochs, d_loss, g_loss))

    print()
    print("Let's see what the generator can generate: ")
    examples = 10
    latent_samples = make_latent_samples(examples, sample_size, window_length)
    generated_vectors = generator.predict_on_batch(latent_samples)

    for i, sample in enumerate(generated_vectors):
        tweet = []
        for sequence in sample:
            nn_vector_index = find_nearest_neighbor(sequence, embeddings_index)
            nn_word = e[nn_vector_index]
            tweet.append(nn_word)
        print("Generated tweet ", str(i), ": ", ' '.join(tweet))



def make_simple_GAN(sample_size,
                    g_hidden_size,
                    d_hidden_size,
                    leaky_alpha,
                    g_learning_rate,
                    d_learning_rate):
    K.clear_session()

    '''
    generator = Sequential([
        Dense(g_hidden_size, input_shape=(sample_size,)),
        LeakyReLU(alpha=leaky_alpha),
        Dense(784),
        Activation('tanh')
    ], name='generator')

    discriminator = Sequential([
        Dense(d_hidden_size, input_shape=(784,)),
        LeakyReLU(alpha=leaky_alpha),
        Dense(1),
        Activation('sigmoid')
    ], name='discriminator')
    '''

    # create a seq2seq model as generator
    generator = Sequential([
        SimpleSeq2Seq(input_dim=sample_size, hidden_dim=g_hidden_size, output_length=window_length, output_dim=n_features)
    ], name='generator')

    # create and fit the SimpleRNN model
    discriminator = Sequential([
        SimpleRNN(units=d_hidden_size, input_shape=(window_length, n_features)),
        Dense(1),
        Activation('sigmoid')
    ], name='discriminator')

    print(generator.summary())
    print()
    print(discriminator.summary())

    gan = Sequential([
        generator,
        discriminator
    ])

    discriminator.compile(optimizer=Adam(lr=d_learning_rate), loss='binary_crossentropy')
    gan.compile(optimizer=Adam(lr=g_learning_rate), loss='binary_crossentropy')

    return gan, generator, discriminator


def make_labels(size):
    return np.ones([size, 1]), np.zeros([size, 1])


def make_trainable(model, trainable):
    for layer in model.layers:
        layer.trainable = trainable


def make_latent_samples(n_samples, sample_size, seq_size):
    # return np.random.uniform(-1, 1, size=(n_samples, sample_size))
    return np.random.normal(loc=0, scale=1, size=(n_samples, seq_size, sample_size))


def get_index(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


if __name__ == '__main__':
    main()