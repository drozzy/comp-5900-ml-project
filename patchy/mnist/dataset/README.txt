This archive contains labeled datasets used during the research for paper:  
Zunaira Jamil, Diana Inkpen, Prasadith Buddhitha, and Kenton White. Monitoring Tweets for Depression to Detect At-risk Users. In Proceedings of the Fourth Workshop on Computational Linguistics and Clinical Psychology - From Linguistic Signal to Clinical Reality (CLPsych 2017), at ACL 2017, Vnacouver, BC, Aug 2017, 
available at: http://www.aclweb.org/anthology/W17-3104

"labeled datasets/60Users_tweet_annotations.csv" contains tweets labeled at tweet level. This dataset was labeled by 2 annotaters. 0 indicates no signs of distress. 1-3 indicate some distress.
"labeled datasets/100Users_tweets.csv" contains tweet-level data for another 100 users.
"labeled datasets/160Users_annotations.csv" contains user-level annotations for all 160 users (whose tweet-level data is available in 60Users_annotations.csv and 100Users_tweets.csv)
"userLevel_testingSplit.csv" contains list of node IDs for users in our test set
"userLevel_trainingSplit.csv" contains list of node IDs for users in our training set.
"tweetLevel_traininsSplit.csv" contains list of tweet IDs for tweets in our training set.
"tweetLevel_testingSplit.csv" contains list of tweets IDs for tweets in our testing set.

NOTE: microsoft excel may not not display complete tweet IDs. 

Kindly cite our paper (mentioned above) if you use these datasets.
