import matplotlib
matplotlib.use('Qt5Agg')

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from keras.datasets import mnist
import argparse
from anogan import gan, anogan
from combiner import combine_images

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def generated_callback(epoch, index, generated):
    # visualize training results
    if index % 20 == 0:
        image = combine_images(generated)
        image = image*127.5+127.5
        cv2.imwrite('./result/'+str(epoch)+"_"+str(index)+".png", image)


def main(args):
    ### 0. prepare data
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    X_train = (X_train.astype(np.float32) - 127.5) / 127.5
    X_test = (X_test.astype(np.float32) - 127.5) / 127.5

    X_train = X_train[:,:,:,None]
    X_test = X_test[:,:,:,None]

    X_test_original = X_test.copy()

    digit = args.digit # Digit to learn

    # Uncomment to train on just one digit - leave commented to train on ALL!
    X_train = X_train[y_train==digit]
    X_test = X_test[y_test==digit]

    print ('train shape:', X_train.shape)

    ### 1. train generator & discriminator
    if args.mode == 'train':
        Model_d, Model_g = gan.train(64, X_train, generated_callback)

    ### 2. test generator
    generated_img = gan.generate(25)
    img = combine_images(generated_img)
    img = (img*127.5)+127.5
    img = img.astype(np.uint8)
    img = cv2.resize(img, None, fx=4, fy=4, interpolation=cv2.INTER_NEAREST)

    ### opencv view
    # cv2.namedWindow('generated', 0)
    # cv2.resizeWindow('generated', 256, 256)
    # cv2.imshow('generated', img)
    # cv2.imwrite('result_latent_10/generator.png', img)
    # cv2.waitKey()

    ### plt view
    # plt.figure(num=0, figsize=(4, 4))
    # plt.title('trained generator')
    # plt.imshow(img, cmap=plt.cm.gray)
    # plt.show()

    img_idx = args.img_idx
    label_idx = args.label_idx

    # Sample from noise or real img, pick one of:
    test_img = X_test_original[y_test==label_idx][img_idx] # One of digits
    #test_img = np.random.uniform(-1,1, (28,28,1)) # Random noisy img

    start = cv2.getTickCount()

    score, query, pred, diff = anomaly_detection(test_img)
    time = (cv2.getTickCount() - start) / cv2.getTickFrequency() * 1000
    print ('%d label, %d : done'%(label_idx, img_idx), '%.2f'%score, '%.2fms'%time)

    def save_bigger_img(img, name):
        img_new = cv2.resize(img, (300, 300), interpolation=cv2.INTER_LINEAR)
        cv2.imwrite(name, img_new)

    save_bigger_img(query, 'query.png')
    save_bigger_img(pred, 'prediction.png')
    save_bigger_img(diff, 'difference.png')

    # t_sne() - TODO: Uncomment to do t_sne

def t_sne():
    ### 4. tsne feature view

    ### t-SNE embedding 
    ### generating anomaly image for test (radom noise image)

    from sklearn.manifold import TSNE

    random_image = np.random.uniform(0, 1, (100, 28, 28, 1))
    print("random noise image")
    plt.figure(4, figsize=(2, 2))
    plt.title('random noise image')
    plt.imshow(random_image[0].reshape(28,28), cmap=plt.cm.gray)

    # intermidieate output of discriminator
    model = anogan.feature_extractor()
    feature_map_of_random = model.predict(random_image, verbose=1)
    feature_map_of_minist = model.predict(X_test_original[y_test != 1][:300], verbose=1)
    feature_map_of_minist_1 = model.predict(X_test[:100], verbose=1)

    # t-SNE for visulization
    output = np.concatenate((feature_map_of_random, feature_map_of_minist, feature_map_of_minist_1))
    output = output.reshape(output.shape[0], -1)
    anomaly_flag = np.array([1]*100+ [0]*300)

    X_embedded = TSNE(n_components=2).fit_transform(output)
    plt.figure(5)
    plt.title("t-SNE embedding on the feature representation")
    plt.scatter(X_embedded[:100,0], X_embedded[:100,1], label='random noise(anomaly)')
    plt.scatter(X_embedded[100:400,0], X_embedded[100:400,1], label='mnist(anomaly)')
    plt.scatter(X_embedded[400:,0], X_embedded[400:,1], label='mnist(normal)')
    plt.legend()
    plt.show()





def anomaly_detection(test_img, g=None, d=None):
    model = anogan.anomaly_detector(g=g, d=d)
    ano_score, similar_img = anogan.compute_anomaly_score(model, test_img.reshape(1, 28, 28, 1), iterations=500, d=d)

    # anomaly area, 255 normalization
    np_residual = test_img.reshape(28,28,1) - similar_img.reshape(28,28,1)
    np_residual = (np_residual + 2)/4

    np_residual = (255*np_residual).astype(np.uint8)
    original_x = (test_img.reshape(28,28,1)*127.5+127.5).astype(np.uint8)
    similar_x = (similar_img.reshape(28,28,1)*127.5+127.5).astype(np.uint8)

    original_x_color = cv2.cvtColor(original_x, cv2.COLOR_GRAY2BGR)
    residual_color = cv2.applyColorMap(np_residual, cv2.COLORMAP_JET)
    show = cv2.addWeighted(original_x_color, 0.3, residual_color, 0.7, 0.)

    return ano_score, original_x, similar_x, show


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--digit', type=int, default=1)
    parser.add_argument('--img_idx', type=int, default=14)
    parser.add_argument('--label_idx', type=int, default=7)
    parser.add_argument('--mode', type=str, default='test', help='train, test')
    args = parser.parse_args()
    main(args)