
<center><img src="./image/overview.png" width=100%></center>
<br>
<center><img src="./image/problem.png" width=100%></center>
<br>
<center><img src="./image/loss.png" width=100%></center>
<br>
<center><img src="./image/tsne.png" width=100%></center>
<br>

-----

| query image | generated similar image | differece |
|:---:|:---:|:---:|
| <img src="./result_latent_10/result_query_0.png" width=100%> | <img src="./result_latent_10/result_pred_0.png" width=100%> | <img src="./result_latent_10/result_diff_0.png" width=100%> |
| <img src="./result_latent_10/result_query_1.png" width=100%> | <img src="./result_latent_10/result_pred_1.png" width=100%> | <img src="./result_latent_10/result_diff_1.png" width=100%> |
| <img src="./result_latent_10/result_query_2.png" width=100%> | <img src="./result_latent_10/result_pred_2.png" width=100%> | <img src="./result_latent_10/result_diff_2.png" width=100%> |
| <img src="./result_latent_10/result_query_4.png" width=100%> | <img src="./result_latent_10/result_pred_4.png" width=100%> | <img src="./result_latent_10/result_diff_4.png" width=100%> |
| <img src="./result_latent_10/result_query_7.png" width=100%> | <img src="./result_latent_10/result_pred_7.png" width=100%> | <img src="./result_latent_10/result_diff_7.png" width=100%> |
| <img src="./result_latent_10/result_query_9.png" width=100%> | <img src="./result_latent_10/result_pred_9.png" width=100%> | <img src="./result_latent_10/result_diff_9.png" width=100%> |


-----
<br><br>

# Patchy 

Anomaly detection in text using GAN.

## Attribution

Code heavily borrowed from AnoGAN keras implementation - Unsupervised anomaly detection with DCGAN: https://github.com/tkwoo/anogan-keras


## Requirements

Run included Makefile to see the options on how to launch the envrionment inside a Docker containe with GPU support.
This project uses the Dockerfile copied from Keras codebase for Docker, see here for more details:

https://github.com/keras-team/keras/tree/75a35032e194a2d065b0071a9e786adf6cee83ea/docker

## Usage  

First, check directory structure

    ├── main.py
    ├── anogan.py 
    ├── weights
        ├── discriminator.h5
        └── generator.h5
    └── result
        └── save the generated images when training

To test this project

    $ python main.py


To train a model

    $ python main.py --mode train

Then, the training steps(image) will be saved 'result' directory


-----------

    usage: main.py [-h] [--img_idx IMG_IDX] 
                        [--label_idx LABEL_IDX] 
                        [--mode MODE]


### Reference

paper : https://arxiv.org/abs/1703.05921 <br>
anogan-keras - improved keras implementation https://github.com/tkwoo/anogan-keras based on yjucho1 below<br>
AnoGAN(code, keras) : https://github.com/yjucho1/anoGAN <br>
AnoGAN(code, tf) : https://github.com/LeeDoYup/AnoGAN <br>