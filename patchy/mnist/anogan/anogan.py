from .gan import generator_model, discriminator_model
import numpy as np
from keras.layers import Input, Dense
from keras.layers.core import Activation
from keras.models import Model
from keras import backend as K

def anomaly_detector(g=None, d=None):
    if g is None:
        g = generator_model()
        g.load_weights('weights/generator.h5')
    intermidiate_model = feature_extractor(d)
    intermidiate_model.trainable = False
    g = Model(inputs=g.layers[1].input, outputs=g.layers[-1].output)
    g.trainable = False

    # Input layer cann't be trained. Add new layer with the same size & distribution
    aInput = Input(shape=(10,))
    gInput = Dense((10), trainable=True)(aInput)
    gInput = Activation('sigmoid')(gInput)
    
    G_out = g(gInput)
    D_out= intermidiate_model(G_out)    
    model = Model(inputs=aInput, outputs=[G_out, D_out])

    def sum_of_residual(y_true, y_pred):
        return K.sum(K.abs(y_true - y_pred))

    model.compile(loss=sum_of_residual, loss_weights= [0.90, 0.10], optimizer='rmsprop')
    
    # batchnorm learning phase fixed (test) : make non trainable
    K.set_learning_phase(0)
    
    return model

def compute_anomaly_score(anomaly_detector, x, iterations=500, d=None):
    z = np.random.uniform(0, 1, size=(1, 10))
    
    intermidiate_model = feature_extractor(d)
    d_x = intermidiate_model.predict(x)

    # learning for changing latent
    loss = anomaly_detector.fit(z, [x, d_x], batch_size=1, epochs=iterations, verbose=0)
    similar_data, _ = anomaly_detector.predict(z)
    
    loss = loss.history['loss'][-1]
    
    return loss, similar_data

def feature_extractor(d=None):
    """ 
    Discriminator intermediate layer feautre extraction
    """
    if d is None:
        d = discriminator_model()
        d.load_weights('weights/discriminator.h5') 
    intermidiate_model = Model(inputs=d.layers[0].input, outputs=d.get_layer('intermediate').output)
    intermidiate_model.compile(loss='binary_crossentropy', optimizer='rmsprop')
    return intermidiate_model
