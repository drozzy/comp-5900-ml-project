Shakespeare or not Shakespeare?
================================

Simple test system to determine whether a given piece of text is "Shakespearian" or not, with the help of a GAN.

Dataset source: Shakespeare plays https://www.kaggle.com/kingburrito666/shakespeare-plays	

Train/test split
================

Original lines have been shuffled and split into train/test split with 90,000 in train and the rest 21,397 in test.

How to run
===========

To build a docker image and run bash, just execute:

    make bash

