"""
Creates embeddings for training or test data.
Call `load_vectors()` to retrieve an array N x 300 of all embeddings.
"""
from pathlib import Path
import os
import sys
from nltk.tokenize import word_tokenize
import nltk
import logging
from sklearn.model_selection import train_test_split
from pyfasttext import FastText
import numpy as np
from tqdm import tqdm
import pickle

MODEL_DIR = '/src/workspace/model'
FASTTEXT_BIN = '/src/workspace/model/cc.en.300.bin'
SHAKESPEARE = '/src/workspace/dataset/Shakespeare.txt'
NUMPY_SAVE_VECS = '/src/workspace/model/cc.en.300'
NUMPY_LOAD_VECS = '/src/workspace/model/cc.en.300.npy'
TEXT_PATCHES = '/src/workspace/model/text_patches.pickle'

PATCH_SIZE = 8

def ensure_model():
	import sys

	model_file = Path(FASTTEXT_BIN)
	if model_file.is_file() or model_file.is_symlink():
		logging.info("FastText model found!")
	else:
		logging.error("Missing FastText model: {}".format(FASTTEXT_BIN))
		logging.error("Please download and unzip it from {}.".format("https://s3-us-west-1.amazonaws.com/fasttext-vectors/cc.en.300.bin.gz"))
		sys.exit(0)
	

def load_model():
	ensure_model()

	nltk.data.path.append(MODEL_DIR)
	nltk.download('punkt', download_dir=MODEL_DIR)

	print("Loading fast text model...")
	model = FastText(FASTTEXT_BIN)
	print("Done.")
	return model

def load_patches(sentences):
	if Path(TEXT_PATCHES).is_file():
		print("Text Patches found! Returning...")
		
		with open(TEXT_PATCHES, 'rb') as f:
			return pickle.load(f)
	else:
		logging.warning("Text Patches do not exist. Creating...")
		model = load_model()
		patches = create_patches(sentences, model)

		print("Saving Text Patches to file.")

		f = open(TEXT_PATCHES, 'wb')
		pickle.dump(patches, f)
		f.close()

		return patches

def create_patches(samples):
	""" N x PATCH_SIZE x 300 """
	patches = []

	for vecs in tqdm(samples):
		
		#vecs = np.array([model.get_numpy_vector(word) for word in word_tokenize(sentence)])
		sentence_patches = array_to_patches(np.array(vecs))
		patches.extend(sentence_patches)

	return patches

def array_to_patches(arr):
	num_patches = int(np.floor(arr.shape[0] / PATCH_SIZE))
	return np.split(arr[:(num_patches * PATCH_SIZE)], num_patches)

def load_vectors(sentences, path):
	"""
	Input:
	N x sentence_string
	Returns a list of lists of word vectors:
	N x num_words_in_a_sentence x 300
	"""
	if Path(path).is_file():
		print("Word vectors found! Returning...")
		
		with open(path, 'rb') as f:
			return pickle.load(f)

	else:
		logging.warning("Word vectors do not exist. Creating...")

		model = load_model()
		vecs = create_vectors(sentences, model)
		model = None
		
		print("Saving numpy to file...")

		f = open(path, 'wb')
		pickle.dump(vecs, f)
		f.close()

		print("Done.")

		return vecs

def create_vectors(arr, model):	
	
	vecs = []

	print("Creating word vectors for training data...")
	for line in tqdm(arr):
		text_vecs = []
		for word in word_tokenize(line):
			text_vecs.append(model.get_numpy_vector(word))
		vecs.append(text_vecs)

	# print("Done.")

	print("Creating numpy array from vector list.")
	# n = np.array(vecs)
		
	# print("Done.")
	return vecs

if __name__ == '__main__':
	vecs = load_vectors()

	print("Vectors loaded. Observe memory consumption.")
	input("Press Enter to exit...")