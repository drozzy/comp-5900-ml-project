from keras.models import Model
from keras.layers import Input, Reshape, Dense, MaxPooling2D, Conv2D, Flatten, LSTM, CuDNNLSTM, TimeDistributed
from keras.layers import Conv2DTranspose, LeakyReLU
from keras.layers.core import Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import RMSprop, Adam
import numpy as np
from tqdm import tqdm
import cv2
from keras.utils. generic_utils import Progbar

TEXT_PATCH_SIZE = 8

def generator_model():
    latent_space_dim = 10
    g_hidden_size = 300
    
    lstm_hidden_space = 64
    leaky_alpha = 0.01
    
    inputs = Input((latent_space_dim,))
    dense = Dense( TEXT_PATCH_SIZE * latent_space_dim)(inputs)    
    reshaped = Reshape((TEXT_PATCH_SIZE, latent_space_dim))(dense)
    #time_distr_dense1 = TimeDistributed(Dense(units=g_hidden_size))(reshaped)
    leaky_relu1 = LeakyReLU(alpha=leaky_alpha)(reshaped)
    lstm1 = CuDNNLSTM(128, return_sequences=True)(leaky_relu1)
    lstm2 = CuDNNLSTM(256, return_sequences=True)(lstm1)
    outputs = CuDNNLSTM(g_hidden_size, return_sequences=True)(lstm2)
    
    model = Model(inputs=[inputs], outputs=[outputs])
    return model

def generate(count):
    g = generator_model()
    g.load_weights('weights/generator.h5')
    noise = np.random.uniform(0, 1, (count, 10))
    generated_images = g.predict(noise)
    return generated_images

def discriminator_model():
    d_hidden_state = 32
    dim_size = 300

    inputs = Input((TEXT_PATCH_SIZE, dim_size))
    lstm1 = CuDNNLSTM(256, return_sequences=True)(inputs)
    lstm2 = CuDNNLSTM(128, return_sequences=True)(lstm1)
    lstm3 = CuDNNLSTM(64)(lstm2)
    dense1 = Dense(32, name='middle')(lstm3)
    act = Activation('tanh')(dense1)
    dense2 = Dense(1)(act)
    outputs = Activation('sigmoid')(dense2)
    
    
    model = Model(inputs=[inputs], outputs=[outputs])
    return model
    

def generator_containing_discriminator(g, d):
    d.trainable = False
    ganInput = Input(shape=(10,))
    x = g(ganInput)
    print(x.shape)
    ganOutput = d(x)
    gan = Model(inputs=ganInput, outputs=ganOutput)
    return gan

def train(BATCH_SIZE, X_train, generated_callback=None, epochs=10):
    """
    @arg generated_callback(epoch, index, generator_output) - callback
         called after every prediction by generator. Can be used
         for visualizing training.
    """
    d_optim = Adam(lr=0.0004)
    g_optim = Adam(lr=0.0002)
    
    d = discriminator_model()
    d.compile(loss='binary_crossentropy', optimizer=d_optim, metrics=['accuracy'])
    print("Discriminator: ", d.summary())
    
    g = generator_model()
    g.compile(loss='binary_crossentropy', optimizer=g_optim)
    print("Generator: ", g.summary())
    
    d_on_g = generator_containing_discriminator(g, d)
    d_on_g.compile(loss='binary_crossentropy', optimizer=g_optim, metrics=['accuracy'])
    print("GAN: ", d_on_g.summary())
    d.trainable = True
    
    
    noise2 = np.random.uniform(0, 1, size=(BATCH_SIZE, 10))

    for epoch in range(epochs):
        print ("Epoch is", epoch)
        n_iter = int(X_train.shape[0]/BATCH_SIZE)
        progress_bar = Progbar(target=n_iter)
        
        for index in range(n_iter):
            # create random noise -> U(0,1) 10 latent vectors
            noise = np.random.uniform(0, 1, size=(BATCH_SIZE, 10))

            # load real data & generate fake data
            image_batch = X_train[index*BATCH_SIZE:(index+1)*BATCH_SIZE]
            generated_images = g.predict(noise, verbose=0)
            
            if generated_callback is not None:
                generated_callback(epoch, index, generated_images)

            # attach label for training the discriminator
            X = np.concatenate((image_batch, generated_images))
            y = np.array([1] * BATCH_SIZE + [0] * BATCH_SIZE)
            
            # train the discriminator
            d_loss = d.train_on_batch(X, y)

            # train the generator
            d.trainable = False
            g_loss = d_on_g.train_on_batch(noise, np.array([1] * BATCH_SIZE))
            d.trainable = True
            progress_bar.update(index, values=[('g',g_loss[0]), ('d',d_loss[0])])
        print ('')

        # save weights for each epoch
        g.save_weights('weights/generator.h5', True)
        d.save_weights('weights/discriminator.h5', True)
    return d, g