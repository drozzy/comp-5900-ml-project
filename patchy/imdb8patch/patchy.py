import embedding
import numpy as np
from anogan import gan
from sklearn.model_selection import train_test_split

def run(train=False):
    
    vecs = embedding.load_vectors()
    (train_data, test_data) = train_test_split(vecs, random_state=42)

    print("Vecs and train/test split.")
    print(vecs.shape)
    print(train_data.shape)
    print(test_data.shape)
    
    if train == True:
        print("Training...")
        gan.train(64, train_data, generated_callback)

def generated_callback(epoch, index, generated):
    print("Callback at epoch=%s, index=%s" % (epoch, index))