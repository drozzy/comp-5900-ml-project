import argparse
import patchy

def main(args):	
	patchy.run(train=args.train)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--train', type=bool, default=False, help='Set to True if you want to train the gan before.')
	args = parser.parse_args()
	main(args)
